var searchData=
[
  ['backcmd_0',['backcmd',['../dd/da3/sh_8c.html#a35f9169f2feb5b3752e1e9c839520166',1,'sh.c']]],
  ['badarg_1',['badarg',['../de/dc0/usertests_8c.html#aa6a891530d4e96cd06654aecd47cfc0b',1,'usertests.c']]],
  ['badwrite_2',['badwrite',['../de/dc0/usertests_8c.html#a0537da3cadbc68402e9842f9c092040d',1,'usertests.c']]],
  ['balloc_3',['balloc',['../d2/d5a/fs_8c.html#a29272227722d57ff937caaffd03455a9',1,'balloc(uint dev):&#160;fs.c'],['../d7/d1f/mkfs_8c.html#a327cdfc7a74165d8922ec6c8ba256906',1,'balloc(int):&#160;mkfs.c']]],
  ['begin_5fop_4',['begin_op',['../d5/d64/defs_8h.html#ac96aa31ffc0500e749c62c4d377c21c9',1,'begin_op(void):&#160;log.c'],['../d7/df8/log_8c.html#ac96aa31ffc0500e749c62c4d377c21c9',1,'begin_op(void):&#160;log.c']]],
  ['bfree_5',['bfree',['../d2/d5a/fs_8c.html#a2800a4b570ae39363cec0613dac92fd7',1,'fs.c']]],
  ['bget_6',['bget',['../dc/de6/bio_8c.html#a69dcd3ce86d958b4024c6746263f3f5b',1,'bio.c']]],
  ['bigargtest_7',['bigargtest',['../de/dc0/usertests_8c.html#a0c37e73fedfc23cf056ead5bcf82a1e7',1,'usertests.c']]],
  ['bigdir_8',['bigdir',['../de/dc0/usertests_8c.html#a8f08332f805e8828b313a7d56577c8b8',1,'usertests.c']]],
  ['bigfile_9',['bigfile',['../de/dc0/usertests_8c.html#a32d7338de0b6c671aa75da138fc6459f',1,'usertests.c']]],
  ['bigwrite_10',['bigwrite',['../de/dc0/usertests_8c.html#a3d9f33ceb3baa32873674da3c4595bcd',1,'usertests.c']]],
  ['binit_11',['binit',['../d5/d64/defs_8h.html#a53cca0ddc98c5f1de37124eca2575a59',1,'binit(void):&#160;bio.c'],['../dc/de6/bio_8c.html#a53cca0ddc98c5f1de37124eca2575a59',1,'binit(void):&#160;bio.c']]],
  ['bmap_12',['bmap',['../d2/d5a/fs_8c.html#a965e83e1fa9b15abf268784ce74181bb',1,'fs.c']]],
  ['bpin_13',['bpin',['../dc/de6/bio_8c.html#ad77f8c0f9c4750b98dc422f279d554a7',1,'bpin(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#a40ab7a772469664bf681fd1ede870d29',1,'bpin(struct buf *):&#160;bio.c']]],
  ['bread_14',['bread',['../dc/de6/bio_8c.html#a30b8d2ef2300ed8e3d879a428fe39898',1,'bread(uint dev, uint blockno):&#160;bio.c'],['../d5/d64/defs_8h.html#a03ff74b99b4c3282b93b122e74d0804b',1,'bread(uint, uint):&#160;bio.c']]],
  ['brelse_15',['brelse',['../dc/de6/bio_8c.html#ab5335aeb503731104314321a78a6d727',1,'brelse(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#aa31ec2f79e0456737a9680270bc1841b',1,'brelse(struct buf *):&#160;bio.c']]],
  ['bsstest_16',['bsstest',['../de/dc0/usertests_8c.html#abdaa4977e75c43ad99e5077c21b35143',1,'usertests.c']]],
  ['bunpin_17',['bunpin',['../dc/de6/bio_8c.html#a8f81c094aeba2d51e78035ec6a16b303',1,'bunpin(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#aef19220c64c1a4dfa158a69030cd40c1',1,'bunpin(struct buf *):&#160;bio.c']]],
  ['bwrite_18',['bwrite',['../dc/de6/bio_8c.html#a63c899c13b176ddf80064d32225e1298',1,'bwrite(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#a1bfd775f14ad3dfee354ee3897ecd28d',1,'bwrite(struct buf *):&#160;bio.c']]],
  ['bzero_19',['bzero',['../d2/d5a/fs_8c.html#ac93f607f4a954323f4c9ea485ca88538',1,'fs.c']]]
];
