var searchData=
[
  ['t0_0',['t0',['../df/d05/structtrapframe.html#a9161c4162ecefcb698a237cfaf2a57a8',1,'trapframe']]],
  ['t1_1',['t1',['../df/d05/structtrapframe.html#a0802f9dd5498dde6e69f42b4ec32c460',1,'trapframe']]],
  ['t2_2',['t2',['../df/d05/structtrapframe.html#ad163c51d2467966412d751a34173a646',1,'trapframe']]],
  ['t3_3',['t3',['../df/d05/structtrapframe.html#a335862231499a59b6ac72531d384938f',1,'trapframe']]],
  ['t4_4',['t4',['../df/d05/structtrapframe.html#aae1b9a9dad559a8d9679c2125886578a',1,'trapframe']]],
  ['t5_5',['t5',['../df/d05/structtrapframe.html#ab2d4e0596c3cfd80dcd51872759c9cbf',1,'trapframe']]],
  ['t6_6',['t6',['../df/d05/structtrapframe.html#af0366806df226474f587ed472dbecb0f',1,'trapframe']]],
  ['ticks_7',['ticks',['../dc/d6f/trap_8c.html#a7fcd6915876e066781399d7b00f1b1f0',1,'ticks():&#160;trap.c'],['../d5/d64/defs_8h.html#a7fcd6915876e066781399d7b00f1b1f0',1,'ticks():&#160;trap.c']]],
  ['tickslock_8',['tickslock',['../dc/d6f/trap_8c.html#a094a4703b62095e2fa469fab3ffea5c7',1,'tickslock():&#160;trap.c'],['../d5/d64/defs_8h.html#a094a4703b62095e2fa469fab3ffea5c7',1,'tickslock():&#160;trap.c']]],
  ['tp_9',['tp',['../df/d05/structtrapframe.html#ac80b7189b1e8ec2f22f916c05b88bcdb',1,'trapframe']]],
  ['trampoline_10',['trampoline',['../de/de9/vm_8c.html#a796aa4017efb6527598a228f4b7cc9ac',1,'trampoline():&#160;vm.c'],['../dc/d6f/trap_8c.html#a796aa4017efb6527598a228f4b7cc9ac',1,'trampoline():&#160;trap.c'],['../d3/dda/proc_8c.html#a796aa4017efb6527598a228f4b7cc9ac',1,'trampoline():&#160;proc.c']]],
  ['trapframe_11',['trapframe',['../de/d48/structproc.html#ab498b20e36164fbbfde1c85d08791dd4',1,'proc']]],
  ['type_12',['type',['../d6/d4d/structvirtio__blk__req.html#a92f65ba3c55d3dbe4798bd7c4d438a2b',1,'virtio_blk_req::type()'],['../d5/db7/structcmd.html#a9b861866e1dec63e694247fb4d976423',1,'cmd::type()'],['../dd/db7/structexeccmd.html#a9db03fe1a8bff63e9205fc98eeb19741',1,'execcmd::type()'],['../d8/dac/structredircmd.html#ac3e4a2de55ca2175ede05a3f49bbb835',1,'redircmd::type()'],['../d6/d94/structpipecmd.html#a75d1d90b6721ac025c0062ec0947aaea',1,'pipecmd::type()'],['../d8/d63/structlistcmd.html#a2db395eea2aa2323b521cd272c6ccc23',1,'listcmd::type()'],['../da/d1f/structbackcmd.html#a38298e998fc63358cc59f1e0dcf85a38',1,'backcmd::type()'],['../da/de7/structstat.html#a01f1b4cd7627d192a7875c9a188e0699',1,'stat::type()'],['../db/dfa/structdinode.html#abf6b2a8476a803284f1c927fb3b82259',1,'dinode::type()'],['../d0/df8/structinode.html#a8d74bec2898785c057111c328d23fda2',1,'inode::type()'],['../d7/d3a/structfile.html#a77b112b03af551a272c64e133752e837',1,'file::type()'],['../de/d4f/structproghdr.html#a715544b91d4a1d09732b9a5d37faec0b',1,'proghdr::type()'],['../d7/da8/structelfhdr.html#a2cd2eaf0c952e30f8196890787ef68fe',1,'elfhdr::type()']]]
];
