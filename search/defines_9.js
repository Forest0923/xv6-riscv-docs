var searchData=
[
  ['n_0',['N',['../d5/de1/forktest_8c.html#a0240ac851181b84ac374872dc5434ee4',1,'forktest.c']]],
  ['nbuf_1',['NBUF',['../d5/d33/param_8h.html#ad51e2f8dffd163c263ec676a268d0f0a',1,'param.h']]],
  ['ncpu_2',['NCPU',['../d5/d33/param_8h.html#a2c4561c4c17cde39101c4e7a40d4492a',1,'param.h']]],
  ['ndev_3',['NDEV',['../d5/d33/param_8h.html#aa564a41c8409694da49b0badf2bb2853',1,'param.h']]],
  ['ndirect_4',['NDIRECT',['../df/d26/fs_8h.html#acd38e9532d4b3623f844b93c012a8e06',1,'fs.h']]],
  ['nelem_5',['NELEM',['../d5/d64/defs_8h.html#afacd331296c10f360da7640e0d68429f',1,'defs.h']]],
  ['nfile_6',['NFILE',['../d5/d33/param_8h.html#a8485f4e81de2537e6a0935626167a775',1,'param.h']]],
  ['nindirect_7',['NINDIRECT',['../df/d26/fs_8h.html#a6d24f098ea2928d61ff80b123d761715',1,'fs.h']]],
  ['ninode_8',['NINODE',['../d5/d33/param_8h.html#adabafa10c7951be7c875cd2ee212be85',1,'param.h']]],
  ['ninodes_9',['NINODES',['../d7/d1f/mkfs_8c.html#ad285a4c453432da6be26e87a581ee35c',1,'mkfs.c']]],
  ['nofile_10',['NOFILE',['../d5/d33/param_8h.html#a80bacbaea8dd6aecf216d85d981bcb21',1,'param.h']]],
  ['nproc_11',['NPROC',['../d5/d33/param_8h.html#a810c5b751df5bb30588613ed91095129',1,'param.h']]],
  ['num_12',['NUM',['../d7/dcd/virtio_8h.html#aaf0952059602752258dccaa015d7b54a',1,'virtio.h']]]
];
