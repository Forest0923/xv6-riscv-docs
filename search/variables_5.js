var searchData=
[
  ['fd_0',['fd',['../d8/dac/structredircmd.html#a1d4b8ba36b5bb4e9e4af58a6d58934a1',1,'redircmd']]],
  ['file_1',['file',['../d8/dac/structredircmd.html#adfbfcf9111724e5b453bab2ed3ee308f',1,'redircmd::file()'],['../d6/d13/file_8c.html#a7cfa5243b3d349a415c7500c962fe9a5',1,'file():&#160;file.c']]],
  ['filesz_2',['filesz',['../de/d4f/structproghdr.html#a76658ed3108eb9f11a088f234aeebe5a',1,'proghdr']]],
  ['flags_3',['flags',['../d7/da8/structelfhdr.html#a2f1e0957c83938630ef0ed074830df03',1,'elfhdr::flags()'],['../de/d4f/structproghdr.html#a42d9ae7fac6cf72da7cabec40e0d4a0a',1,'proghdr::flags()'],['../d7/d7a/structvirtq__desc.html#abab306cac4dd25876ae8b3698ec5beca',1,'virtq_desc::flags()'],['../dc/d3a/structvirtq__avail.html#a61dc09ea0af6f735eca1fbdca97a3e8c',1,'virtq_avail::flags()'],['../d3/daa/structvirtq__used.html#a086c639fe7d960ab901cddf99dea3786',1,'virtq_used::flags()']]],
  ['free_4',['free',['../d0/dea/structdisk.html#ae89ed6cb81d9debb051aee32c7171811',1,'disk::free()'],['../d9/d3e/virtio__disk_8c.html#a0608361a8f53a287e3f3a3c7e46ed1ef',1,'free():&#160;virtio_disk.c']]],
  ['freeblock_5',['freeblock',['../d7/d1f/mkfs_8c.html#a8d3a0b59d5f6e59c8b7c0bbdab41ab15',1,'mkfs.c']]],
  ['freeinode_6',['freeinode',['../d7/d1f/mkfs_8c.html#acee9059b25c8d81ef2b9cfedded17d48',1,'mkfs.c']]],
  ['freelist_7',['freelist',['../d4/def/kalloc_8c.html#a25f1f0e27ad1cafebbde0b8b7455afb4',1,'kalloc.c']]],
  ['freep_8',['freep',['../df/d5d/umalloc_8c.html#a11c3a364c10375c46e28e33c0030cc8c',1,'umalloc.c']]],
  ['fsfd_9',['fsfd',['../d7/d1f/mkfs_8c.html#a44f12de41bc5a5ada9e5fff19c18201c',1,'mkfs.c']]],
  ['ftable_10',['ftable',['../d6/d13/file_8c.html#af824064f5278e855b7f579372357feee',1,'file.c']]]
];
