var searchData=
[
  ['link_0',['link',['../d8/ddb/user_8h.html#a9f0ffa429d1a466322fb9b5bb084f828',1,'user.h']]],
  ['linktest_1',['linktest',['../de/dc0/usertests_8c.html#a5176bd5eeb504ea880d06fe4b37b435f',1,'usertests.c']]],
  ['linkunlink_2',['linkunlink',['../de/dc0/usertests_8c.html#a629f797cb47f2788aa13d87d9d168e32',1,'usertests.c']]],
  ['listcmd_3',['listcmd',['../dd/da3/sh_8c.html#ab7f6ee47c6aa12d1222cd2626afd3af6',1,'sh.c']]],
  ['loadseg_4',['loadseg',['../df/dc9/exec_8c.html#abb3ab09beb7087bc97d4be6a2cb4a425',1,'loadseg(pde_t *pgdir, uint64 addr, struct inode *ip, uint offset, uint sz):&#160;exec.c'],['../df/dc9/exec_8c.html#a110e7b95731421e43e806a144009fd2b',1,'loadseg(pagetable_t pagetable, uint64 va, struct inode *ip, uint offset, uint sz):&#160;exec.c']]],
  ['log_5fwrite_5',['log_write',['../d5/d64/defs_8h.html#a270d0050dc50965f4f851717841ad33c',1,'log_write(struct buf *):&#160;log.c'],['../d7/df8/log_8c.html#a7eacb0fbebe5ce4c7d3ddea15908b13d',1,'log_write(struct buf *b):&#160;log.c']]],
  ['ls_6',['ls',['../de/d77/ls_8c.html#a0dd2caa6531a27dcd755c46e814b63af',1,'ls.c']]]
];
