var searchData=
[
  ['getcmd_0',['getcmd',['../dd/da3/sh_8c.html#a0ee5a9418fea2f92036f5367a4101fe8',1,'sh.c']]],
  ['getmycpu_1',['getmycpu',['../d5/d64/defs_8h.html#a94b32bc58412c8b12d0ec8bbf24fa8d4',1,'defs.h']]],
  ['getpid_2',['getpid',['../d8/ddb/user_8h.html#a939cb25a305fe68aad9b365077f1a8c7',1,'user.h']]],
  ['gets_3',['gets',['../db/d8a/ulib_8c.html#a5d99411ae3770ce242b82a1d01b47164',1,'gets(char *buf, int max):&#160;ulib.c'],['../d8/ddb/user_8h.html#a7dc53f126f9818785d5824bbb155f7a4',1,'gets(char *, int max):&#160;ulib.c']]],
  ['gettoken_4',['gettoken',['../dd/da3/sh_8c.html#aaf1dedac6db801eab8d23f2acb3b1272',1,'sh.c']]],
  ['go_5',['go',['../d7/d85/grind_8c.html#aff7a90e612392dc698242480a6ff44d9',1,'grind.c']]],
  ['gp_6',['gp',['../df/d05/structtrapframe.html#a2bc8c6ffea9920c30e3f258cd9625aa2',1,'trapframe']]],
  ['grep_7',['grep',['../d5/dcb/grep_8c.html#aefb6587871b683544f860d4e2b518718',1,'grep.c']]],
  ['grep_2ec_8',['grep.c',['../d5/dcb/grep_8c.html',1,'']]],
  ['grind_2ec_9',['grind.c',['../d7/d85/grind_8c.html',1,'']]],
  ['growproc_10',['growproc',['../d5/d64/defs_8h.html#acb02e9289fb8a1017c3455b137a9bccd',1,'growproc(int):&#160;proc.c'],['../d3/dda/proc_8c.html#a9c16214741f4fcd088e5eea468709328',1,'growproc(int n):&#160;proc.c']]]
];
