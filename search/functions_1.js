var searchData=
[
  ['acquire_0',['acquire',['../d3/d2d/spinlock_8c.html#aed377f16a085b00de3a4b32392adbdfb',1,'acquire(struct spinlock *lk):&#160;spinlock.c'],['../d5/d64/defs_8h.html#afe4ef8638f1ecb962a6e67fb086ee3b8',1,'acquire(struct spinlock *):&#160;spinlock.c']]],
  ['acquiresleep_1',['acquiresleep',['../d5/d64/defs_8h.html#aecd4639fe2f9aaad8e8cee2b5e0688c3',1,'acquiresleep(struct sleeplock *):&#160;sleeplock.c'],['../d8/dd1/sleeplock_8c.html#aaad2d7a86d4859982dd9a132f5128ff2',1,'acquiresleep(struct sleeplock *lk):&#160;sleeplock.c']]],
  ['alloc3_5fdesc_2',['alloc3_desc',['../d9/d3e/virtio__disk_8c.html#a1ec2edfe03003d478868a1d3964118a8',1,'virtio_disk.c']]],
  ['alloc_5fdesc_3',['alloc_desc',['../d9/d3e/virtio__disk_8c.html#a68ab0a3df2b532b21d556685eb663864',1,'virtio_disk.c']]],
  ['allocpid_4',['allocpid',['../d3/dda/proc_8c.html#aa8d34090b3dff1dcd42684b6bc50af96',1,'proc.c']]],
  ['allocproc_5',['allocproc',['../d3/dda/proc_8c.html#a63162470dd94e51243a480b3e20c86a4',1,'proc.c']]],
  ['argaddr_6',['argaddr',['../db/dd8/syscall_8c.html#a665cd458558260a2c1b7df9b8fc10335',1,'argaddr(int n, uint64 *ip):&#160;syscall.c'],['../d5/d64/defs_8h.html#a17560c17977f439fef7dceb48be10c7a',1,'argaddr(int, uint64 *):&#160;syscall.c']]],
  ['argfd_7',['argfd',['../d1/d14/sysfile_8c.html#a875f88d58107de20db21cfcb64a09e0a',1,'sysfile.c']]],
  ['argint_8',['argint',['../d5/d64/defs_8h.html#a75bc8d8c7ea0b4b39d4f470e18e0dba7',1,'argint(int, int *):&#160;syscall.c'],['../db/dd8/syscall_8c.html#ade56ef2176f85cd61e7b91b400e7d4d3',1,'argint(int n, int *ip):&#160;syscall.c']]],
  ['argptest_9',['argptest',['../de/dc0/usertests_8c.html#aaf2caa9fd7c2ffaae574c43cfe4c66c2',1,'usertests.c']]],
  ['argraw_10',['argraw',['../db/dd8/syscall_8c.html#aeade91811d3fe8e73a7fa5d56bea46e0',1,'syscall.c']]],
  ['argstr_11',['argstr',['../d5/d64/defs_8h.html#a445e1db2b8499001fa6bd3a7bb3e59b0',1,'argstr(int, char *, int):&#160;syscall.c'],['../db/dd8/syscall_8c.html#a8c04dc78dae64b8e0a29972d4119d67c',1,'argstr(int n, char *buf, int max):&#160;syscall.c']]],
  ['atoi_12',['atoi',['../db/d8a/ulib_8c.html#a30670a60464f77af17dfb353353d6df8',1,'atoi(const char *s):&#160;ulib.c'],['../d8/ddb/user_8h.html#a4e157d18591ba54c92e4da99a3d1ccae',1,'atoi(const char *):&#160;ulib.c']]]
];
