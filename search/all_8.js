var searchData=
[
  ['head_0',['head',['../dc/de6/bio_8c.html#aa6e692c16f1b909f5cb2a1832cf43430',1,'bio.c']]],
  ['header_1',['header',['../d3/d5d/unionheader.html',1,'']]],
  ['header_2',['Header',['../df/d5d/umalloc_8c.html#a1a2bfaef0959ba21a357a6f8feaa477b',1,'umalloc.c']]],
  ['holding_3',['holding',['../d5/d64/defs_8h.html#ac44b13cc76bf4040e3baf34df75ff230',1,'holding(struct spinlock *):&#160;spinlock.c'],['../d3/d2d/spinlock_8c.html#a100f8d71ef722adfbdea7b8df56bee30',1,'holding(struct spinlock *lk):&#160;spinlock.c']]],
  ['holdingsleep_4',['holdingsleep',['../d5/d64/defs_8h.html#afa76133bc67c6026376d630da9b53b68',1,'holdingsleep(struct sleeplock *):&#160;sleeplock.c'],['../d8/dd1/sleeplock_8c.html#a6fdbe54cbecd8fc67d74793b27adcf05',1,'holdingsleep(struct sleeplock *lk):&#160;sleeplock.c']]],
  ['hour_5',['hour',['../d3/de7/structrtcdate.html#ac601418b8ff95c35e8fddb47dd3fc77b',1,'rtcdate']]]
];
