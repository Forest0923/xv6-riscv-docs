var searchData=
[
  ['machine_0',['machine',['../d7/da8/structelfhdr.html#a17113c58d39b044bb1ae78733a8c68fc',1,'elfhdr']]],
  ['magic_1',['magic',['../d7/da8/structelfhdr.html#a28ee8116d69b533277311c5f3773b6b2',1,'elfhdr::magic()'],['../df/d3d/structsuperblock.html#aae96cc3156725d1311f55717a51a0e11',1,'superblock::magic()']]],
  ['main_2',['main',['../de/d77/ls_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;ls.c'],['../d5/dcb/grep_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;grep.c'],['../d5/de1/forktest_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;forktest.c'],['../da/daf/echo_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;echo.c'],['../d1/d8f/cat_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;cat.c'],['../d7/d1f/mkfs_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;mkfs.c'],['../d7/d2a/start_8c.html#acdef7a1fd863a6d3770c1268cb06add3',1,'main():&#160;main.c'],['../d0/d29/main_8c.html#acdef7a1fd863a6d3770c1268cb06add3',1,'main():&#160;main.c'],['../d7/d85/grind_8c.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;grind.c'],['../d8/d60/init_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;init.c'],['../d2/d1d/kill_8c.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;kill.c'],['../d0/d37/ln_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;ln.c'],['../db/d1f/mkdir_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;mkdir.c'],['../dc/d14/rm_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;rm.c'],['../dd/da3/sh_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;sh.c'],['../d4/dbf/stressfs_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;stressfs.c'],['../de/dc0/usertests_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;usertests.c'],['../d3/d7a/wc_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main(int argc, char *argv[]):&#160;wc.c'],['../dd/da4/zombie_8c.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;zombie.c']]],
  ['main_2ec_3',['main.c',['../d0/d29/main_8c.html',1,'']]],
  ['major_4',['major',['../db/dfa/structdinode.html#aca8272002020f48219df175c986db257',1,'dinode::major()'],['../d0/df8/structinode.html#a34af7242018a977dace5730683850875',1,'inode::major()'],['../d7/d3a/structfile.html#ae1092bf40571eef31b7c4c57bd5f69c2',1,'file::major()'],['../d2/d4d/file_8h.html#ab9488f99ec7a2bebb56093a9a336e0ac',1,'major():&#160;file.h']]],
  ['make_5fsatp_5',['MAKE_SATP',['../d1/d4e/riscv_8h.html#a7f4cdb98920ced01e9f94ee090d46164',1,'riscv.h']]],
  ['malloc_6',['malloc',['../d8/ddb/user_8h.html#a6ce12ff2e91d62ac939f6cad0788501f',1,'malloc(uint):&#160;umalloc.c'],['../df/d5d/umalloc_8c.html#ac131c16d1dcb6bea908d0237c82e981d',1,'malloc(uint nbytes):&#160;umalloc.c']]],
  ['manywrites_7',['manywrites',['../de/dc0/usertests_8c.html#a1efefa367089dff8a3fa255470175ecd',1,'usertests.c']]],
  ['mappages_8',['mappages',['../de/de9/vm_8c.html#a06d3f4561c42a60312750ff980622469',1,'mappages(pagetable_t pagetable, uint64 va, uint64 size, uint64 pa, int perm):&#160;vm.c'],['../d5/d64/defs_8h.html#a849f47b75c76d08161d615daacf6ad5b',1,'mappages(pagetable_t, uint64, uint64, uint64, int):&#160;vm.c']]],
  ['match_9',['match',['../d5/dcb/grep_8c.html#a21a26bd0e26e51f529e7fcc1f7ffbb0c',1,'grep.c']]],
  ['matchhere_10',['matchhere',['../d5/dcb/grep_8c.html#a179fc8565cee7d1e5c9cf742e61f78e4',1,'grep.c']]],
  ['matchstar_11',['matchstar',['../d5/dcb/grep_8c.html#a1775658a8b4077f2284310c5cdab3ef1',1,'grep.c']]],
  ['maxarg_12',['MAXARG',['../d5/d33/param_8h.html#a4c171d1ccc50f0b6ce7ad2f475eeba32',1,'param.h']]],
  ['maxargs_13',['MAXARGS',['../dd/da3/sh_8c.html#a41101847771d39a4f0a7f9395061c629',1,'sh.c']]],
  ['maxfile_14',['MAXFILE',['../df/d26/fs_8h.html#a714b2485a6dd312e37226e1f833728a9',1,'fs.h']]],
  ['maxopblocks_15',['MAXOPBLOCKS',['../d5/d33/param_8h.html#a31cde324007b4e52bbba7079ec5e5f45',1,'param.h']]],
  ['maxpath_16',['MAXPATH',['../d5/d33/param_8h.html#a2d1edde91ea515b363277f4bbdc3244d',1,'param.h']]],
  ['maxva_17',['MAXVA',['../d1/d4e/riscv_8h.html#a781d1689a393d7c6e4243349cfdf6ce8',1,'riscv.h']]],
  ['maxvaplus_18',['MAXVAplus',['../de/dc0/usertests_8c.html#a53d860e6fb0a973cbcfd7aa2cb9aa495',1,'usertests.c']]],
  ['mem_19',['mem',['../de/dc0/usertests_8c.html#ac4d934dd4104e9f291a6179f05d2ced3',1,'usertests.c']]],
  ['memcmp_20',['memcmp',['../d8/ddb/user_8h.html#acdb4d3f48d2fb0a834722b1107d2b284',1,'memcmp(const void *, const void *, uint):&#160;string.c'],['../db/d8a/ulib_8c.html#a4abb057e0958f3401a36b5b00c29ef12',1,'memcmp(const void *s1, const void *s2, uint n):&#160;ulib.c'],['../d1/db0/string_8c.html#aa70027bbf4ef8a7388150c5b374f9b16',1,'memcmp(const void *v1, const void *v2, uint n):&#160;string.c'],['../d5/d64/defs_8h.html#acdb4d3f48d2fb0a834722b1107d2b284',1,'memcmp(const void *, const void *, uint):&#160;string.c']]],
  ['memcpy_21',['memcpy',['../d8/ddb/user_8h.html#ae77fd61aedd10405f78309de80e43cf5',1,'memcpy(void *, const void *, uint):&#160;string.c'],['../db/d8a/ulib_8c.html#abe8c241a3db904f06adffc49a96f6fc7',1,'memcpy(void *dst, const void *src, uint n):&#160;ulib.c'],['../d1/db0/string_8c.html#abe8c241a3db904f06adffc49a96f6fc7',1,'memcpy(void *dst, const void *src, uint n):&#160;string.c']]],
  ['memlayout_2eh_22',['memlayout.h',['../d8/da9/memlayout_8h.html',1,'']]],
  ['memmove_23',['memmove',['../d5/d64/defs_8h.html#ac152fff34fe03e497d35b1d4c47f1445',1,'memmove(void *, const void *, uint):&#160;string.c'],['../d1/db0/string_8c.html#a07f97e2ed1ce37955192d52de8f4911f',1,'memmove(void *dst, const void *src, uint n):&#160;string.c'],['../db/d8a/ulib_8c.html#a6d50c17bc17085392dd935f098573def',1,'memmove(void *vdst, const void *vsrc, int n):&#160;ulib.c'],['../d8/ddb/user_8h.html#ab0cb468c5236ca7d15ec8f455a9dbdc4',1,'memmove(void *, const void *, int):&#160;ulib.c']]],
  ['memset_24',['memset',['../d8/ddb/user_8h.html#a0098886a849eef10803e9f35ceec61b9',1,'memset(void *, int, uint):&#160;string.c'],['../db/d8a/ulib_8c.html#aa9cdc2409c15ccd5ee3b2031576f4093',1,'memset(void *dst, int c, uint n):&#160;ulib.c'],['../d1/db0/string_8c.html#aa9cdc2409c15ccd5ee3b2031576f4093',1,'memset(void *dst, int c, uint n):&#160;string.c'],['../d5/d64/defs_8h.html#a0098886a849eef10803e9f35ceec61b9',1,'memset(void *, int, uint):&#160;string.c']]],
  ['memsz_25',['memsz',['../de/d4f/structproghdr.html#ad4021cc9473683982ea1cc639536bf00',1,'proghdr']]],
  ['mie_5fmeie_26',['MIE_MEIE',['../d1/d4e/riscv_8h.html#ab4e2e4b72ec1e13b315dcdc910f5595b',1,'riscv.h']]],
  ['mie_5fmsie_27',['MIE_MSIE',['../d1/d4e/riscv_8h.html#acdb82cffb46911d1a236dcf651b24b05',1,'riscv.h']]],
  ['mie_5fmtie_28',['MIE_MTIE',['../d1/d4e/riscv_8h.html#a88f0c7a5a13f0570ecbaa43b912b0fe4',1,'riscv.h']]],
  ['min_29',['min',['../d2/d5a/fs_8c.html#ac6afabdc09a49a433ee19d8a9486056d',1,'min():&#160;fs.c'],['../d7/d1f/mkfs_8c.html#ac6afabdc09a49a433ee19d8a9486056d',1,'min():&#160;mkfs.c']]],
  ['minor_30',['minor',['../d2/d4d/file_8h.html#a316b71ec9ad36e62161fea5966645750',1,'minor():&#160;file.h'],['../d0/df8/structinode.html#a37878866e7905b666db2aa33076076a2',1,'inode::minor()'],['../db/dfa/structdinode.html#ae97965f85e7353313f85035e8fc63495',1,'dinode::minor()']]],
  ['minute_31',['minute',['../d3/de7/structrtcdate.html#a5984e264f332d7634912db2716472aa7',1,'rtcdate']]],
  ['mkdev_32',['mkdev',['../d2/d4d/file_8h.html#ab6ef49a09d742608e42117ee6c185252',1,'file.h']]],
  ['mkdir_33',['mkdir',['../d8/ddb/user_8h.html#a034fad25eff1c559bf33767e574d1d62',1,'user.h']]],
  ['mkdir_2ec_34',['mkdir.c',['../db/d1f/mkdir_8c.html',1,'']]],
  ['mkfs_2ec_35',['mkfs.c',['../d7/d1f/mkfs_8c.html',1,'']]],
  ['mknod_36',['mknod',['../d8/ddb/user_8h.html#a3b9ff823fbf509b3d200bd3d5c238365',1,'user.h']]],
  ['mode_37',['mode',['../d8/dac/structredircmd.html#a36b522983b6a0c0efdaea471b08d120b',1,'redircmd']]],
  ['month_38',['month',['../d3/de7/structrtcdate.html#a3c509170b31d76f828681c2df54bf0b1',1,'rtcdate']]],
  ['morecore_39',['morecore',['../df/d5d/umalloc_8c.html#a744822bd4d229ed09b4019c6efddaccb',1,'umalloc.c']]],
  ['mstatus_5fmie_40',['MSTATUS_MIE',['../d1/d4e/riscv_8h.html#a225cb34e3b991318fa87f090cfc3fc5f',1,'riscv.h']]],
  ['mstatus_5fmpp_5fm_41',['MSTATUS_MPP_M',['../d1/d4e/riscv_8h.html#aa6621785868a067469e73ac9babeed99',1,'riscv.h']]],
  ['mstatus_5fmpp_5fmask_42',['MSTATUS_MPP_MASK',['../d1/d4e/riscv_8h.html#a16d6d0c403a36d5c6be7915bb7b830b7',1,'riscv.h']]],
  ['mstatus_5fmpp_5fs_43',['MSTATUS_MPP_S',['../d1/d4e/riscv_8h.html#a484f2297ebe1535484c13f12f70e602a',1,'riscv.h']]],
  ['mstatus_5fmpp_5fu_44',['MSTATUS_MPP_U',['../d1/d4e/riscv_8h.html#aebc8dae20b3f619d03f0291db287f6de',1,'riscv.h']]],
  ['mycpu_45',['mycpu',['../d5/d64/defs_8h.html#ad427959ad025dabd8cd393b27ec39160',1,'mycpu(void):&#160;proc.c'],['../d3/dda/proc_8c.html#ad427959ad025dabd8cd393b27ec39160',1,'mycpu(void):&#160;proc.c']]],
  ['myproc_46',['myproc',['../d5/d64/defs_8h.html#a3d2327db1e34643f4a9e6a07c8cc62c6',1,'myproc():&#160;proc.c'],['../d3/dda/proc_8c.html#a41af0935f3989aae450cf8988cd9c3a9',1,'myproc(void):&#160;proc.c']]]
];
