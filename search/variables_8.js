var searchData=
[
  ['id_0',['id',['../db/d2b/structvirtq__used__elem.html#ac86c713ff86cb074249fe90d51532f07',1,'virtq_used_elem']]],
  ['idx_1',['idx',['../dc/d3a/structvirtq__avail.html#a92970c139f246ff844c38801e6c7417d',1,'virtq_avail::idx()'],['../d3/daa/structvirtq__used.html#a230e4d0fb3a2db726154f9099e1b38bf',1,'virtq_used::idx()']]],
  ['info_2',['info',['../d0/dea/structdisk.html#afd4dface987d5981c59803afc7e5c9b2',1,'disk::info()'],['../d9/d3e/virtio__disk_8c.html#a10880a3279ccfb3be997289e528e4ad8',1,'info():&#160;virtio_disk.c']]],
  ['initcode_3',['initcode',['../d3/dda/proc_8c.html#a5d34564fa6460ba9eaab2ff109f9a4ec',1,'proc.c']]],
  ['initproc_4',['initproc',['../d3/dda/proc_8c.html#aefa2374374b48494732e6494ccca9b89',1,'proc.c']]],
  ['ino_5',['ino',['../da/de7/structstat.html#abf15624517ed5d79d0fa2a5553a68e25',1,'stat']]],
  ['inode_6',['inode',['../d2/d5a/fs_8c.html#a6baaf26dd83b71b8d684c5d54a709e31',1,'fs.c']]],
  ['inodestart_7',['inodestart',['../df/d3d/structsuperblock.html#adde361528f3905445974301b424611c1',1,'superblock']]],
  ['intena_8',['intena',['../db/d62/structcpu.html#a26fc271fea8af30d67fc2ae22ef0a82f',1,'cpu']]],
  ['inum_9',['inum',['../d0/df8/structinode.html#a8acc2b9df0bfc6856da62925763db7fe',1,'inode::inum()'],['../d5/de2/structdirent.html#a68698c303a46d2a34232a2226629ac79',1,'dirent::inum()']]],
  ['ip_10',['ip',['../d7/d3a/structfile.html#a4fd095f927715574dc2a4f243690e508',1,'file']]],
  ['itable_11',['itable',['../d2/d5a/fs_8c.html#a8fd23309f97e9c7b623d7c1a4ea72763',1,'fs.c']]]
];
