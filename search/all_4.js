var searchData=
[
  ['data_0',['data',['../d9/dd0/structbuf.html#ab0ec38784ab94ed35e575cf6d33912d2',1,'buf::data()'],['../d8/d53/structpipe.html#ab02ae9fa0b8b092512c28c7c080f0c7b',1,'pipe::data()']]],
  ['date_2eh_1',['date.h',['../db/d96/date_8h.html',1,'']]],
  ['day_2',['day',['../d3/de7/structrtcdate.html#a476a4a04d68d88b1515123aa24af8a4d',1,'rtcdate']]],
  ['defs_2eh_3',['defs.h',['../d5/d64/defs_8h.html',1,'']]],
  ['desc_4',['desc',['../d9/d3e/virtio__disk_8c.html#a67cc2ad049e2ba5c3f6f617170e2e251',1,'desc():&#160;virtio_disk.c'],['../d0/dea/structdisk.html#a847007649d1e24c3992da06ad63a8a02',1,'disk::desc()']]],
  ['dev_5',['dev',['../d9/dd0/structbuf.html#ac96082c2b5f22133ac7092ef81487227',1,'buf::dev()'],['../d0/df8/structinode.html#a121742a89c4531f03a7de1613d5be605',1,'inode::dev()'],['../d0/d4a/structlog.html#aebeeb9df7326549fb5d8b7221c9b0aa3',1,'log::dev()'],['../da/de7/structstat.html#a14ef4f85e6fb86bf296360361d0f393b',1,'stat::dev()']]],
  ['devintr_6',['devintr',['../dc/d6f/trap_8c.html#a89e201737ec867c2f24606180d0b78da',1,'trap.c']]],
  ['devsw_7',['devsw',['../d2/d4d/file_8h.html#aef498b4c2cbc1a4286c67ff5f20afec9',1,'devsw():&#160;file.c'],['../d6/d13/file_8c.html#aadbb32b41c0d0e9c19d6d8fa3a0a6502',1,'devsw():&#160;file.c'],['../de/d6a/structdevsw.html',1,'devsw']]],
  ['die_8',['die',['../d7/d1f/mkfs_8c.html#a363ae7b35a07daeca974c86d1c39b732',1,'mkfs.c']]],
  ['digits_9',['digits',['../db/d8a/kernel_2printf_8c.html#a00c037531fdedd7c1d0397b1b77928d3',1,'digits():&#160;printf.c'],['../d7/dea/user_2printf_8c.html#a00c037531fdedd7c1d0397b1b77928d3',1,'digits():&#160;printf.c']]],
  ['dinode_10',['dinode',['../db/dfa/structdinode.html',1,'']]],
  ['dirent_11',['dirent',['../d5/de2/structdirent.html',1,'']]],
  ['dirfile_12',['dirfile',['../de/dc0/usertests_8c.html#afa489c47bc58d4e5dcb08de29e8757e6',1,'usertests.c']]],
  ['dirlink_13',['dirlink',['../d5/d64/defs_8h.html#ae4ccea0aa02557162963e597737f665a',1,'dirlink(struct inode *, char *, uint):&#160;fs.c'],['../d2/d5a/fs_8c.html#a69a135a0e8a06d9f306d77ebc0c1f7a0',1,'dirlink(struct inode *dp, char *name, uint inum):&#160;fs.c']]],
  ['dirlookup_14',['dirlookup',['../d5/d64/defs_8h.html#aef19d16d1d20d8f7cdcd8ee0d98fc13f',1,'dirlookup(struct inode *, char *, uint *):&#160;fs.c'],['../d2/d5a/fs_8c.html#a5341068f021beac8cf0032b97d231e6f',1,'dirlookup(struct inode *dp, char *name, uint *poff):&#160;fs.c']]],
  ['dirsiz_15',['DIRSIZ',['../df/d26/fs_8h.html#a48246fb9e5cb7f6a71ebc9ebc2f06562',1,'fs.h']]],
  ['dirtest_16',['dirtest',['../de/dc0/usertests_8c.html#a6adaf047ed8547cecd76b23591c65637',1,'usertests.c']]],
  ['disk_17',['disk',['../d9/dd0/structbuf.html#a598c840a233e368937d47cd1b7a5f3a2',1,'buf::disk()'],['../d0/dea/structdisk.html',1,'disk']]],
  ['do_5frand_18',['do_rand',['../d7/d85/grind_8c.html#ad75a8575f3fc5f4e51bc4bc2cb3983a3',1,'grind.c']]],
  ['dup_19',['dup',['../d8/ddb/user_8h.html#a530ea95b243a1bf148adaf399e2a6292',1,'user.h']]]
];
