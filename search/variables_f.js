var searchData=
[
  ['r_0',['r',['../d0/d56/console_8c.html#af10fa12dd785c91e29528fbdc50cf9af',1,'console.c']]],
  ['ra_1',['ra',['../d7/dfe/structcontext.html#a83de548cc0c3c80dd1706881fe0681e0',1,'context::ra()'],['../df/d05/structtrapframe.html#a7591004220324950b43c274e38990472',1,'trapframe::ra()']]],
  ['rand_5fnext_2',['rand_next',['../d7/d85/grind_8c.html#ac9036d7a7a5f49817a4cf3fba068dd68',1,'grind.c']]],
  ['read_3',['read',['../de/d6a/structdevsw.html#ad1bc3472e10f06db3a220d4864e71fd5',1,'devsw']]],
  ['readable_4',['readable',['../d7/d3a/structfile.html#a0c5c8eced8bc562dbbecc8d450a6b646',1,'file']]],
  ['readopen_5',['readopen',['../d8/d53/structpipe.html#a7bdc57b39ef97dda61e468ad9e8dbfba',1,'pipe']]],
  ['ref_6',['ref',['../d7/d3a/structfile.html#a41c818f828adea488058bca63e4df23f',1,'file::ref()'],['../d0/df8/structinode.html#a6a519028ee67f805482b3e1725bf09c5',1,'inode::ref()']]],
  ['refcnt_7',['refcnt',['../d9/dd0/structbuf.html#aaf5efe777371aaeb9944508fd52adda5',1,'buf']]],
  ['reserved_8',['reserved',['../d6/d4d/structvirtio__blk__req.html#a18b0629921a69adec6a09f0295c6a273',1,'virtio_blk_req']]],
  ['right_9',['right',['../d6/d94/structpipecmd.html#a882e2847aa340ae4e201d2e49921b47e',1,'pipecmd::right()'],['../d8/d63/structlistcmd.html#a063a3a59362ddfafb7c0e4c848d4e8c5',1,'listcmd::right()']]],
  ['ring_10',['ring',['../dc/d3a/structvirtq__avail.html#a8ed63fc0f5c396299d19dc017d17ed42',1,'virtq_avail::ring()'],['../d3/daa/structvirtq__used.html#a8b96e7afd54d127f9f9cd1974f09a3fa',1,'virtq_used::ring()']]]
];
