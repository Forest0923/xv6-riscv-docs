var searchData=
[
  ['chan_0',['chan',['../de/d48/structproc.html#a03048a49756c2243576208ba4ec5fbd4',1,'proc']]],
  ['cmd_1',['cmd',['../d8/dac/structredircmd.html#ad0e2824b83cc8a1da99b2922025011dc',1,'redircmd::cmd()'],['../da/d1f/structbackcmd.html#a74a697014e2d3a17710e530fca0e8432',1,'backcmd::cmd()']]],
  ['committing_2',['committing',['../d0/d4a/structlog.html#afc034b98b98897c179ca8fae8e2ee181',1,'log']]],
  ['cons_3',['cons',['../d0/d56/console_8c.html#a799a95b1ef986c63508f73813078ca23',1,'console.c']]],
  ['context_4',['context',['../db/d62/structcpu.html#aca23ddda183c196b640b0206a79daffa',1,'cpu::context()'],['../de/d48/structproc.html#adcab25f82e163328458284a5fcf14879',1,'proc::context()']]],
  ['cpu_5',['cpu',['../d1/d08/structspinlock.html#a290ae772c8ccb9e8c1580204c31a7f88',1,'spinlock']]],
  ['cpus_6',['cpus',['../d3/dda/proc_8c.html#a6d2633e73724907b582dfe6938ed7bb9',1,'cpus():&#160;proc.c'],['../df/d03/proc_8h.html#a6d2633e73724907b582dfe6938ed7bb9',1,'cpus():&#160;proc.c']]],
  ['cwd_7',['cwd',['../de/d48/structproc.html#a493bc338ce008a838eef521972a35257',1,'proc']]]
];
