var searchData=
[
  ['paddr_0',['paddr',['../de/d4f/structproghdr.html#a1c177d0d568fde3ab859e505be35aff0',1,'proghdr']]],
  ['pages_1',['pages',['../d0/dea/structdisk.html#a1efe154eb0b0426351aecae7e2943dcd',1,'disk::pages()'],['../d9/d3e/virtio__disk_8c.html#ac3b88cd8d0eb2119dc863f883170530d',1,'pages():&#160;virtio_disk.c']]],
  ['pagetable_2',['pagetable',['../de/d48/structproc.html#a5683a72d5b43bc15ed748cc3757baaaa',1,'proc']]],
  ['panicked_3',['panicked',['../d1/d87/uart_8c.html#a072db1ca2fe81b2147220705072a3f2e',1,'panicked():&#160;printf.c'],['../db/d8a/kernel_2printf_8c.html#a072db1ca2fe81b2147220705072a3f2e',1,'panicked():&#160;printf.c']]],
  ['parent_4',['parent',['../de/d48/structproc.html#a14ea8849701ffafba4d142725de154d4',1,'proc']]],
  ['phentsize_5',['phentsize',['../d7/da8/structelfhdr.html#ac636a4a9c4c61933c6044275ed687bc9',1,'elfhdr']]],
  ['phnum_6',['phnum',['../d7/da8/structelfhdr.html#a3eff58d58a3ee83aa53d7ffdebdb6b5b',1,'elfhdr']]],
  ['phoff_7',['phoff',['../d7/da8/structelfhdr.html#a5412f9ac7a3bd90e0789d256e0f7a465',1,'elfhdr']]],
  ['pid_8',['pid',['../de/d48/structproc.html#acf2bdf54d1f957ccbcdc987007029944',1,'proc::pid()'],['../d9/dd0/structsleeplock.html#a99a4c6a784956ab1c391a12af475a55e',1,'sleeplock::pid()']]],
  ['pid_5flock_9',['pid_lock',['../d3/dda/proc_8c.html#a068d54c415464a7024425e2d205bb540',1,'proc.c']]],
  ['pipe_10',['pipe',['../d7/d3a/structfile.html#a19d83a8d6cb47902fe8c762d2798c198',1,'file']]],
  ['pr_11',['pr',['../db/d8a/kernel_2printf_8c.html#ad9b3cb1779724887349543614b50342b',1,'printf.c']]],
  ['prev_12',['prev',['../d9/dd0/structbuf.html#a930cab1e1b3751795d31bfd0291dff4a',1,'buf']]],
  ['proc_13',['proc',['../db/d62/structcpu.html#a9e71a6265904fd644875a9ea5a413c89',1,'cpu::proc()'],['../d3/dda/proc_8c.html#a5780b0050ca664736fd1cc9696624ce5',1,'proc():&#160;proc.c']]],
  ['ptr_14',['ptr',['../d3/d5d/unionheader.html#adcb7a02e18836885c802789f6a6c99fd',1,'header']]]
];
