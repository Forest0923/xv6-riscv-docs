var searchData=
[
  ['either_5fcopyin_0',['either_copyin',['../d3/dda/proc_8c.html#aa465ecaaf1b2444743f6bc44db85a3d4',1,'either_copyin(void *dst, int user_src, uint64 src, uint64 len):&#160;proc.c'],['../d5/d64/defs_8h.html#aa465ecaaf1b2444743f6bc44db85a3d4',1,'either_copyin(void *dst, int user_src, uint64 src, uint64 len):&#160;proc.c']]],
  ['either_5fcopyout_1',['either_copyout',['../d5/d64/defs_8h.html#aebf67644e81ce595d92b70ae78f4f640',1,'either_copyout(int user_dst, uint64 dst, void *src, uint64 len):&#160;proc.c'],['../d3/dda/proc_8c.html#aebf67644e81ce595d92b70ae78f4f640',1,'either_copyout(int user_dst, uint64 dst, void *src, uint64 len):&#160;proc.c']]],
  ['end_5fop_2',['end_op',['../d5/d64/defs_8h.html#ac0f12be0ca0de555e60b27b06a57a65b',1,'end_op(void):&#160;log.c'],['../d7/df8/log_8c.html#ac0f12be0ca0de555e60b27b06a57a65b',1,'end_op(void):&#160;log.c']]],
  ['exec_3',['exec',['../d8/ddb/user_8h.html#aa7b4aae4a12acd187e23396214aeca47',1,'exec(char *, char **):&#160;exec.c'],['../df/dc9/exec_8c.html#ace32454ed0d37834dcb1cb4f8b727e6e',1,'exec(char *path, char **argv):&#160;exec.c'],['../d5/d64/defs_8h.html#aa7b4aae4a12acd187e23396214aeca47',1,'exec(char *, char **):&#160;exec.c']]],
  ['execcmd_4',['execcmd',['../dd/da3/sh_8c.html#a2d41b4e10301b6da6504cb0cc0940eae',1,'sh.c']]],
  ['execout_5',['execout',['../de/dc0/usertests_8c.html#acdffbca927ed842fee09d0a8df4c20f9',1,'usertests.c']]],
  ['exectest_6',['exectest',['../de/dc0/usertests_8c.html#ae40c0c2096ab7a9d208b46e181cafbcc',1,'usertests.c']]],
  ['exit_7',['exit',['../d5/d64/defs_8h.html#a6f255d924f7a6bb2c4be0c8c2f2d9ce3',1,'exit(int):&#160;proc.c'],['../d3/dda/proc_8c.html#a55e99c539cf7723ec15e856b7e0a8cee',1,'exit(int status):&#160;proc.c'],['../d8/ddb/user_8h.html#a33797246a6278adbce4656e21c7e6622',1,'exit(int) __attribute__((noreturn)):&#160;proc.c']]],
  ['exitiputtest_8',['exitiputtest',['../de/dc0/usertests_8c.html#ae1f42bc95a7bb195aae0e79d8709eabf',1,'usertests.c']]],
  ['exitwait_9',['exitwait',['../de/dc0/usertests_8c.html#ac4cb1a0e3ee4c1c6e6193ccc36242e0a',1,'usertests.c']]]
];
