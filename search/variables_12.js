var searchData=
[
  ['uart_5ftx_5fbuf_0',['uart_tx_buf',['../d1/d87/uart_8c.html#ad4b0fd8a4041634c29ad1ea1049c773b',1,'uart.c']]],
  ['uart_5ftx_5flock_1',['uart_tx_lock',['../d1/d87/uart_8c.html#af5350390d26f3176afd986d20f0fdf6c',1,'uart.c']]],
  ['uart_5ftx_5fr_2',['uart_tx_r',['../d1/d87/uart_8c.html#a9681707c9c08f5b0262e2b6c0fee762c',1,'uart.c']]],
  ['uart_5ftx_5fw_3',['uart_tx_w',['../d1/d87/uart_8c.html#a8a0e1da0da5c007af3f48687c7ded7af',1,'uart.c']]],
  ['uninit_4',['uninit',['../de/dc0/usertests_8c.html#a7def0d79bfddb934aaf3fe627c97daf3',1,'usertests.c']]],
  ['unused_5',['unused',['../dc/d3a/structvirtq__avail.html#a5c38f1dd694e9d0cd643db70449095a0',1,'virtq_avail']]],
  ['used_6',['used',['../d0/dea/structdisk.html#a4821a066fac7cb6a8afab9023849c194',1,'disk::used()'],['../d9/d3e/virtio__disk_8c.html#ac7f9ed91798ae3b0141bdad487ef4f78',1,'used():&#160;virtio_disk.c']]],
  ['used_5fidx_7',['used_idx',['../d0/dea/structdisk.html#a5c4dc4614e9f8297f9daf23febdb5050',1,'disk::used_idx()'],['../d9/d3e/virtio__disk_8c.html#a9bdb5c26bead044def1a004fab37eb00',1,'used_idx():&#160;virtio_disk.c']]],
  ['userret_8',['userret',['../dc/d6f/trap_8c.html#af564f710557d70393b9fb1cce4e194d7',1,'trap.c']]],
  ['uservec_9',['uservec',['../dc/d6f/trap_8c.html#aa6435376c8373232f9561dfb25e7ca28',1,'trap.c']]]
];
