var searchData=
[
  ['o_5fcreate_0',['O_CREATE',['../d7/d9f/fcntl_8h.html#ab40a23079c3b9a7e25ffdc8108c7fb02',1,'fcntl.h']]],
  ['o_5frdonly_1',['O_RDONLY',['../d7/d9f/fcntl_8h.html#a7a68c9ffaac7dbcd652225dd7c06a54b',1,'fcntl.h']]],
  ['o_5frdwr_2',['O_RDWR',['../d7/d9f/fcntl_8h.html#abb0586253488ee61072b73557eeb873b',1,'fcntl.h']]],
  ['o_5ftrunc_3',['O_TRUNC',['../d7/d9f/fcntl_8h.html#ad1d67e453fb3031f40f8cd3403773813',1,'fcntl.h']]],
  ['o_5fwronly_4',['O_WRONLY',['../d7/d9f/fcntl_8h.html#a11b644a8526139c4cc1850dac1271ced',1,'fcntl.h']]],
  ['off_5',['off',['../de/d4f/structproghdr.html#a1bf7a6d04c0c0783b5e9615b2fe441ed',1,'proghdr::off()'],['../d7/d3a/structfile.html#a94a911be6cc1b1326728392d8b40150d',1,'file::off()']]],
  ['ofile_6',['ofile',['../de/d48/structproc.html#a4a9eb0352efe3fc097c91fccfaac50bd',1,'proc']]],
  ['open_7',['open',['../d8/ddb/user_8h.html#a2c4414339f388561554c2deab11a1a07',1,'user.h']]],
  ['openiputtest_8',['openiputtest',['../de/dc0/usertests_8c.html#a2739b2a9b9b56febdb9faecefc6777de',1,'usertests.c']]],
  ['opentest_9',['opentest',['../de/dc0/usertests_8c.html#ab1c27ca3045e290f44c6797060859288',1,'usertests.c']]],
  ['ops_10',['ops',['../d0/dea/structdisk.html#a81fabb9b98c82fe2a7a8a68a1fec2474',1,'disk::ops()'],['../d9/d3e/virtio__disk_8c.html#a5580404502a1641dea5aee5ba4b393f1',1,'ops():&#160;virtio_disk.c']]],
  ['outstanding_11',['outstanding',['../d0/d4a/structlog.html#addfc1fc09a124978bd7e2a23a19d733d',1,'log']]]
];
