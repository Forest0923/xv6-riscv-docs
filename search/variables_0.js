var searchData=
[
  ['a0_0',['a0',['../df/d05/structtrapframe.html#a1bcf5526e15f7cc0481e1176b7311156',1,'trapframe']]],
  ['a1_1',['a1',['../df/d05/structtrapframe.html#af7cc5b359816d994a1f8fc7c6f11ed5f',1,'trapframe']]],
  ['a2_2',['a2',['../df/d05/structtrapframe.html#a95cfa033f310a9f8e502d2feb4f498a4',1,'trapframe']]],
  ['a3_3',['a3',['../df/d05/structtrapframe.html#a72ae7a3b5822d482b9815d0169713feb',1,'trapframe']]],
  ['a4_4',['a4',['../df/d05/structtrapframe.html#ac9426b8ce623bfa88259b29aa2d8a5b3',1,'trapframe']]],
  ['a5_5',['a5',['../df/d05/structtrapframe.html#a15b018821c84e403482b33991f388e98',1,'trapframe']]],
  ['a6_6',['a6',['../df/d05/structtrapframe.html#ab48bca1f1c83fd8245cbd43bad6b7dc8',1,'trapframe']]],
  ['a7_7',['a7',['../df/d05/structtrapframe.html#a00d5c4e8acde9da8f3fdef321f2a2517',1,'trapframe']]],
  ['addr_8',['addr',['../d7/d7a/structvirtq__desc.html#af6a1fd6e0db135022c1097e218ef8872',1,'virtq_desc']]],
  ['addrs_9',['addrs',['../d0/df8/structinode.html#a7ba4ab7e52404b80d6d854678715ae30',1,'inode::addrs()'],['../db/dfa/structdinode.html#a705729b3a39c10c0ba6927fc5e4e0563',1,'dinode::addrs()']]],
  ['align_10',['align',['../de/d4f/structproghdr.html#acf62e36dd34df6e6809330e1462c40f1',1,'proghdr']]],
  ['argv_11',['argv',['../dd/db7/structexeccmd.html#a00e53af4bc0e28bbd6842bdceaee1ea9',1,'execcmd::argv()'],['../d8/d60/init_8c.html#abd1a2cf54950f450187ef24c1cdcac0c',1,'argv():&#160;init.c']]],
  ['avail_12',['avail',['../d0/dea/structdisk.html#a30100a76c329c659c80b39723aca1ca7',1,'disk::avail()'],['../d9/d3e/virtio__disk_8c.html#aed19c9f6a54d6c994ca036854f82b484',1,'avail():&#160;virtio_disk.c']]]
];
