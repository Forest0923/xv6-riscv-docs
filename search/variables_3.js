var searchData=
[
  ['data_0',['data',['../d9/dd0/structbuf.html#ab0ec38784ab94ed35e575cf6d33912d2',1,'buf::data()'],['../d8/d53/structpipe.html#ab02ae9fa0b8b092512c28c7c080f0c7b',1,'pipe::data()']]],
  ['day_1',['day',['../d3/de7/structrtcdate.html#a476a4a04d68d88b1515123aa24af8a4d',1,'rtcdate']]],
  ['desc_2',['desc',['../d0/dea/structdisk.html#a847007649d1e24c3992da06ad63a8a02',1,'disk::desc()'],['../d9/d3e/virtio__disk_8c.html#a67cc2ad049e2ba5c3f6f617170e2e251',1,'desc():&#160;virtio_disk.c']]],
  ['dev_3',['dev',['../d9/dd0/structbuf.html#ac96082c2b5f22133ac7092ef81487227',1,'buf::dev()'],['../d0/df8/structinode.html#a121742a89c4531f03a7de1613d5be605',1,'inode::dev()'],['../d0/d4a/structlog.html#aebeeb9df7326549fb5d8b7221c9b0aa3',1,'log::dev()'],['../da/de7/structstat.html#a14ef4f85e6fb86bf296360361d0f393b',1,'stat::dev()']]],
  ['devsw_4',['devsw',['../d6/d13/file_8c.html#aadbb32b41c0d0e9c19d6d8fa3a0a6502',1,'devsw():&#160;file.c'],['../d2/d4d/file_8h.html#aef498b4c2cbc1a4286c67ff5f20afec9',1,'devsw():&#160;file.c']]],
  ['digits_5',['digits',['../db/d8a/kernel_2printf_8c.html#a00c037531fdedd7c1d0397b1b77928d3',1,'digits():&#160;printf.c'],['../d7/dea/user_2printf_8c.html#a00c037531fdedd7c1d0397b1b77928d3',1,'digits():&#160;printf.c']]],
  ['disk_6',['disk',['../d9/dd0/structbuf.html#a598c840a233e368937d47cd1b7a5f3a2',1,'buf']]]
];
