var searchData=
[
  ['uartgetc_0',['uartgetc',['../d1/d87/uart_8c.html#a3e356b0ecad6aaa537241ba8aec2ba96',1,'uartgetc(void):&#160;uart.c'],['../d5/d64/defs_8h.html#a3e356b0ecad6aaa537241ba8aec2ba96',1,'uartgetc(void):&#160;uart.c']]],
  ['uartinit_1',['uartinit',['../d5/d64/defs_8h.html#a79fa7b73d0d61fdd15d30768a395437d',1,'uartinit(void):&#160;uart.c'],['../d1/d87/uart_8c.html#a79fa7b73d0d61fdd15d30768a395437d',1,'uartinit(void):&#160;uart.c']]],
  ['uartintr_2',['uartintr',['../d5/d64/defs_8h.html#aa64047002b0e84e2611ebf7dc46b7c99',1,'uartintr(void):&#160;uart.c'],['../d1/d87/uart_8c.html#aa64047002b0e84e2611ebf7dc46b7c99',1,'uartintr(void):&#160;uart.c']]],
  ['uartputc_3',['uartputc',['../d5/d64/defs_8h.html#a571626618a1f05ff6854802e936845d6',1,'uartputc(int):&#160;uart.c'],['../d1/d87/uart_8c.html#a55840fa098ac21df6535d6ac91956d07',1,'uartputc(int c):&#160;uart.c']]],
  ['uartputc_5fsync_4',['uartputc_sync',['../d5/d64/defs_8h.html#a38a0c267fad6453ced866ad6f62f4542',1,'uartputc_sync(int):&#160;uart.c'],['../d1/d87/uart_8c.html#a189b832050e3bbe4c5e0a15db377b8ed',1,'uartputc_sync(int c):&#160;uart.c']]],
  ['uartstart_5',['uartstart',['../d1/d87/uart_8c.html#a1ab97ed6cacbe03a526afff5a2d6b66e',1,'uart.c']]],
  ['unlink_6',['unlink',['../d8/ddb/user_8h.html#a4ade26dbdb332f64c44daf49ba949ff4',1,'user.h']]],
  ['unlinkread_7',['unlinkread',['../de/dc0/usertests_8c.html#aa08388b12e1dc4286a78440a0d82af1c',1,'usertests.c']]],
  ['uptime_8',['uptime',['../d8/ddb/user_8h.html#ab637d1dd2fecad1cb0c91a42597a8dbc',1,'user.h']]],
  ['userinit_9',['userinit',['../d5/d64/defs_8h.html#a81c8a6a0cae413bc81aa223f7f7b7205',1,'userinit(void):&#160;proc.c'],['../d3/dda/proc_8c.html#a81c8a6a0cae413bc81aa223f7f7b7205',1,'userinit(void):&#160;proc.c']]],
  ['usertrap_10',['usertrap',['../dc/d6f/trap_8c.html#acfa5000a11c20f9f7f8775248e1a8075',1,'trap.c']]],
  ['usertrapret_11',['usertrapret',['../d5/d64/defs_8h.html#ab247cbaa295acd0662cf88b188f23453',1,'usertrapret(void):&#160;trap.c'],['../dc/d6f/trap_8c.html#ab247cbaa295acd0662cf88b188f23453',1,'usertrapret(void):&#160;trap.c']]],
  ['uvmalloc_12',['uvmalloc',['../d5/d64/defs_8h.html#ac290e03f03f8a2dc4c1235446b2f61a9',1,'uvmalloc(pagetable_t, uint64, uint64):&#160;vm.c'],['../de/de9/vm_8c.html#a16f86b9b2e4a67b7e15206a498f4db7a',1,'uvmalloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz):&#160;vm.c']]],
  ['uvmclear_13',['uvmclear',['../d5/d64/defs_8h.html#ab1ec9349e3ef4ecef4ef79f1a2db30ed',1,'uvmclear(pagetable_t, uint64):&#160;vm.c'],['../de/de9/vm_8c.html#a3907e428e633b6c4b03a439b20f122b4',1,'uvmclear(pagetable_t pagetable, uint64 va):&#160;vm.c']]],
  ['uvmcopy_14',['uvmcopy',['../d5/d64/defs_8h.html#af19ef1cdd26725159a05761b883c842d',1,'uvmcopy(pagetable_t, pagetable_t, uint64):&#160;vm.c'],['../de/de9/vm_8c.html#aed8e1a2b6e2bb91bbdce4af655424c77',1,'uvmcopy(pagetable_t old, pagetable_t new, uint64 sz):&#160;vm.c']]],
  ['uvmcreate_15',['uvmcreate',['../de/de9/vm_8c.html#a0861cf789c153af3703fc9098cc8dea5',1,'uvmcreate():&#160;vm.c'],['../d5/d64/defs_8h.html#a468042c18aebd88046d157462e1ba5f7',1,'uvmcreate(void):&#160;vm.c']]],
  ['uvmdealloc_16',['uvmdealloc',['../d5/d64/defs_8h.html#a4871e2f8346140fd1246b8f0557d3f3a',1,'uvmdealloc(pagetable_t, uint64, uint64):&#160;vm.c'],['../de/de9/vm_8c.html#ae938a1d801ced861bdd2043772f6a476',1,'uvmdealloc(pagetable_t pagetable, uint64 oldsz, uint64 newsz):&#160;vm.c']]],
  ['uvmfree_17',['uvmfree',['../d5/d64/defs_8h.html#a8064fdaa325d6a3ad61f7157202382c3',1,'uvmfree(pagetable_t, uint64):&#160;vm.c'],['../de/de9/vm_8c.html#a3c431d0d80fc92190b3e74b89235c7cc',1,'uvmfree(pagetable_t pagetable, uint64 sz):&#160;vm.c']]],
  ['uvminit_18',['uvminit',['../d5/d64/defs_8h.html#a7d120bdeeadcef42e21b2367b0f8359a',1,'uvminit(pagetable_t, uchar *, uint):&#160;vm.c'],['../de/de9/vm_8c.html#ab1b3507c96554254d27972d88869fdcd',1,'uvminit(pagetable_t pagetable, uchar *src, uint sz):&#160;vm.c']]],
  ['uvmunmap_19',['uvmunmap',['../d5/d64/defs_8h.html#a4fbf40e2c26f6a6111bd0669ef5e8236',1,'uvmunmap(pagetable_t, uint64, uint64, int):&#160;vm.c'],['../de/de9/vm_8c.html#ac94cfce8e654e647f83ae7ccd285ae8b',1,'uvmunmap(pagetable_t pagetable, uint64 va, uint64 npages, int do_free):&#160;vm.c']]]
];
