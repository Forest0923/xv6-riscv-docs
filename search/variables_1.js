var searchData=
[
  ['b_0',['b',['../d0/dea/structdisk.html#a0f33659319699168b62924bbb8557927',1,'disk::b()'],['../d9/d3e/virtio__disk_8c.html#a478a43f541f7e29b957eaaef78d660d4',1,'b():&#160;virtio_disk.c']]],
  ['base_1',['base',['../df/d5d/umalloc_8c.html#a877e065dfc67f9bd937b962b8b968638',1,'umalloc.c']]],
  ['bcache_2',['bcache',['../dc/de6/bio_8c.html#a7725a9957148b7740449a091ea12b0e4',1,'bio.c']]],
  ['block_3',['block',['../d4/d6c/structlogheader.html#a020db7fe04c7ce6b8f4aee2092576c2c',1,'logheader']]],
  ['blockno_4',['blockno',['../d9/dd0/structbuf.html#a756b2bcc88008bef7f21d688aa4a7a48',1,'buf']]],
  ['bmapstart_5',['bmapstart',['../df/d3d/structsuperblock.html#a3c815dda5be6bda609389e76434171cc',1,'superblock']]],
  ['buf_6',['buf',['../dc/de6/bio_8c.html#a72ee90c61d41547b10a533c219e081c6',1,'buf():&#160;bio.c'],['../d0/d56/console_8c.html#aa427837782b05b05204809dfba33c8f5',1,'buf():&#160;console.c'],['../d1/d8f/cat_8c.html#a78a05cf530dddc61e7a26aefe187bd31',1,'buf():&#160;cat.c'],['../d5/dcb/grep_8c.html#ac75fce8692fd1d41a8985f6aacc4a175',1,'buf():&#160;grep.c'],['../de/dc0/usertests_8c.html#a6d8aa5809fc463704493cee4c455227c',1,'buf():&#160;usertests.c'],['../d3/d7a/wc_8c.html#a78a05cf530dddc61e7a26aefe187bd31',1,'buf():&#160;wc.c']]]
];
