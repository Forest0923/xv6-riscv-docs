var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyz",
  1: "bcdefhilprstv",
  2: "bcdefgiklmprstuvwz",
  3: "_abcdefghiklmnoprstuvwxy",
  4: "abcdefghiklmnoprstuvwxyz",
  5: "ahpu",
  6: "p",
  7: "frsuz",
  8: "bcdefiklmnoprstuvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

