var searchData=
[
  ['b_0',['b',['../d0/dea/structdisk.html#a0f33659319699168b62924bbb8557927',1,'disk::b()'],['../d9/d3e/virtio__disk_8c.html#a478a43f541f7e29b957eaaef78d660d4',1,'b():&#160;virtio_disk.c']]],
  ['back_1',['BACK',['../dd/da3/sh_8c.html#ab303ee384877c80cb8855bf0113faf88',1,'sh.c']]],
  ['backcmd_2',['backcmd',['../da/d1f/structbackcmd.html',1,'backcmd'],['../dd/da3/sh_8c.html#a35f9169f2feb5b3752e1e9c839520166',1,'backcmd(struct cmd *subcmd):&#160;sh.c']]],
  ['backspace_3',['BACKSPACE',['../d0/d56/console_8c.html#a629568514359445d2fbda71d70eeb1ce',1,'console.c']]],
  ['badarg_4',['badarg',['../de/dc0/usertests_8c.html#aa6a891530d4e96cd06654aecd47cfc0b',1,'usertests.c']]],
  ['badwrite_5',['badwrite',['../de/dc0/usertests_8c.html#a0537da3cadbc68402e9842f9c092040d',1,'usertests.c']]],
  ['balloc_6',['balloc',['../d7/d1f/mkfs_8c.html#a327cdfc7a74165d8922ec6c8ba256906',1,'balloc(int):&#160;mkfs.c'],['../d2/d5a/fs_8c.html#a29272227722d57ff937caaffd03455a9',1,'balloc(uint dev):&#160;fs.c']]],
  ['base_7',['base',['../df/d5d/umalloc_8c.html#a877e065dfc67f9bd937b962b8b968638',1,'umalloc.c']]],
  ['bblock_8',['BBLOCK',['../df/d26/fs_8h.html#a080eb729016328ec6da85bfe125fc04c',1,'fs.h']]],
  ['bcache_9',['bcache',['../dc/de6/bio_8c.html#a7725a9957148b7740449a091ea12b0e4',1,'bio.c']]],
  ['begin_5fop_10',['begin_op',['../d7/df8/log_8c.html#ac96aa31ffc0500e749c62c4d377c21c9',1,'begin_op(void):&#160;log.c'],['../d5/d64/defs_8h.html#ac96aa31ffc0500e749c62c4d377c21c9',1,'begin_op(void):&#160;log.c']]],
  ['bfree_11',['bfree',['../d2/d5a/fs_8c.html#a2800a4b570ae39363cec0613dac92fd7',1,'fs.c']]],
  ['bget_12',['bget',['../dc/de6/bio_8c.html#a69dcd3ce86d958b4024c6746263f3f5b',1,'bio.c']]],
  ['bigargtest_13',['bigargtest',['../de/dc0/usertests_8c.html#a0c37e73fedfc23cf056ead5bcf82a1e7',1,'usertests.c']]],
  ['bigdir_14',['bigdir',['../de/dc0/usertests_8c.html#a8f08332f805e8828b313a7d56577c8b8',1,'usertests.c']]],
  ['bigfile_15',['bigfile',['../de/dc0/usertests_8c.html#a32d7338de0b6c671aa75da138fc6459f',1,'usertests.c']]],
  ['bigwrite_16',['bigwrite',['../de/dc0/usertests_8c.html#a3d9f33ceb3baa32873674da3c4595bcd',1,'usertests.c']]],
  ['binit_17',['binit',['../d5/d64/defs_8h.html#a53cca0ddc98c5f1de37124eca2575a59',1,'binit(void):&#160;bio.c'],['../dc/de6/bio_8c.html#a53cca0ddc98c5f1de37124eca2575a59',1,'binit(void):&#160;bio.c']]],
  ['bio_2ec_18',['bio.c',['../dc/de6/bio_8c.html',1,'']]],
  ['block_19',['block',['../d4/d6c/structlogheader.html#a020db7fe04c7ce6b8f4aee2092576c2c',1,'logheader']]],
  ['blockno_20',['blockno',['../d9/dd0/structbuf.html#a756b2bcc88008bef7f21d688aa4a7a48',1,'buf']]],
  ['bmap_21',['bmap',['../d2/d5a/fs_8c.html#a965e83e1fa9b15abf268784ce74181bb',1,'fs.c']]],
  ['bmapstart_22',['bmapstart',['../df/d3d/structsuperblock.html#a3c815dda5be6bda609389e76434171cc',1,'superblock']]],
  ['bpb_23',['BPB',['../df/d26/fs_8h.html#a3c88fd21abb83ef11461ce750c466828',1,'fs.h']]],
  ['bpin_24',['bpin',['../dc/de6/bio_8c.html#ad77f8c0f9c4750b98dc422f279d554a7',1,'bpin(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#a40ab7a772469664bf681fd1ede870d29',1,'bpin(struct buf *):&#160;bio.c']]],
  ['bread_25',['bread',['../dc/de6/bio_8c.html#a30b8d2ef2300ed8e3d879a428fe39898',1,'bread(uint dev, uint blockno):&#160;bio.c'],['../d5/d64/defs_8h.html#a03ff74b99b4c3282b93b122e74d0804b',1,'bread(uint, uint):&#160;bio.c']]],
  ['brelse_26',['brelse',['../dc/de6/bio_8c.html#ab5335aeb503731104314321a78a6d727',1,'brelse(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#aa31ec2f79e0456737a9680270bc1841b',1,'brelse(struct buf *):&#160;bio.c']]],
  ['bsize_27',['BSIZE',['../df/d26/fs_8h.html#a403cf3149c084cea115b85c90721039a',1,'fs.h']]],
  ['bsstest_28',['bsstest',['../de/dc0/usertests_8c.html#abdaa4977e75c43ad99e5077c21b35143',1,'usertests.c']]],
  ['buf_29',['buf',['../d9/dd0/structbuf.html',1,'buf'],['../d3/d7a/wc_8c.html#a78a05cf530dddc61e7a26aefe187bd31',1,'buf():&#160;wc.c'],['../de/dc0/usertests_8c.html#a6d8aa5809fc463704493cee4c455227c',1,'buf():&#160;usertests.c'],['../d5/dcb/grep_8c.html#ac75fce8692fd1d41a8985f6aacc4a175',1,'buf():&#160;grep.c'],['../d1/d8f/cat_8c.html#a78a05cf530dddc61e7a26aefe187bd31',1,'buf():&#160;cat.c'],['../d0/d56/console_8c.html#aa427837782b05b05204809dfba33c8f5',1,'buf():&#160;console.c'],['../dc/de6/bio_8c.html#a72ee90c61d41547b10a533c219e081c6',1,'buf():&#160;bio.c']]],
  ['buf_2eh_30',['buf.h',['../d6/d6d/buf_8h.html',1,'']]],
  ['bufsz_31',['BUFSZ',['../de/dc0/usertests_8c.html#ab26c26cbf7a0c97d8960c3cfb413f882',1,'usertests.c']]],
  ['bunpin_32',['bunpin',['../dc/de6/bio_8c.html#a8f81c094aeba2d51e78035ec6a16b303',1,'bunpin(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#aef19220c64c1a4dfa158a69030cd40c1',1,'bunpin(struct buf *):&#160;bio.c']]],
  ['bwrite_33',['bwrite',['../dc/de6/bio_8c.html#a63c899c13b176ddf80064d32225e1298',1,'bwrite(struct buf *b):&#160;bio.c'],['../d5/d64/defs_8h.html#a1bfd775f14ad3dfee354ee3897ecd28d',1,'bwrite(struct buf *):&#160;bio.c']]],
  ['bzero_34',['bzero',['../d2/d5a/fs_8c.html#ac93f607f4a954323f4c9ea485ca88538',1,'fs.c']]]
];
