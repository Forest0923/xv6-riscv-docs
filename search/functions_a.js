var searchData=
[
  ['kalloc_0',['kalloc',['../d4/def/kalloc_8c.html#a1f8404abe5ee4315bf0477bbc69c884f',1,'kalloc(void):&#160;kalloc.c'],['../d5/d64/defs_8h.html#a1f8404abe5ee4315bf0477bbc69c884f',1,'kalloc(void):&#160;kalloc.c']]],
  ['kerneltrap_1',['kerneltrap',['../dc/d6f/trap_8c.html#aee583f80cd74fdae19ca29d44705b560',1,'trap.c']]],
  ['kernelvec_2',['kernelvec',['../dc/d6f/trap_8c.html#a7dc924dadc66d3d60cc0ec5e47833fc1',1,'trap.c']]],
  ['kernmem_3',['kernmem',['../de/dc0/usertests_8c.html#a70688fdb3356835e12dfabb324eba447',1,'usertests.c']]],
  ['kfree_4',['kfree',['../d5/d64/defs_8h.html#acee1960bdb3a19cb495341ec725cafef',1,'kfree(void *):&#160;kalloc.c'],['../d4/def/kalloc_8c.html#ac905e2b33cf453b44b2c00ff8c5332bc',1,'kfree(void *pa):&#160;kalloc.c']]],
  ['kill_5',['kill',['../d5/d64/defs_8h.html#ab893e9671d6bfe2b2604002a50639f21',1,'kill(int):&#160;proc.c'],['../d3/dda/proc_8c.html#a650cf0caaaa8b75f653c1c92818d03a4',1,'kill(int pid):&#160;proc.c'],['../d8/ddb/user_8h.html#ab893e9671d6bfe2b2604002a50639f21',1,'kill(int):&#160;proc.c']]],
  ['killstatus_6',['killstatus',['../de/dc0/usertests_8c.html#ab14f9683fedac6774061cc45814818e7',1,'usertests.c']]],
  ['kinit_7',['kinit',['../d5/d64/defs_8h.html#a71d803d14f0e1b0682e2c20e7c2a7f7a',1,'kinit(void):&#160;kalloc.c'],['../d4/def/kalloc_8c.html#aea491e5b831017ff628370356b1e233d',1,'kinit():&#160;kalloc.c']]],
  ['kvminit_8',['kvminit',['../d5/d64/defs_8h.html#ad3db4f1bfd99af5323f5de011cce97ef',1,'kvminit(void):&#160;vm.c'],['../de/de9/vm_8c.html#ad3db4f1bfd99af5323f5de011cce97ef',1,'kvminit(void):&#160;vm.c']]],
  ['kvminithart_9',['kvminithart',['../d5/d64/defs_8h.html#a6ffb295d0891f85c14ddc23fdff6b6f1',1,'kvminithart(void):&#160;vm.c'],['../de/de9/vm_8c.html#a50b4defd5589721d63b46e55e957e863',1,'kvminithart():&#160;vm.c']]],
  ['kvmmake_10',['kvmmake',['../de/de9/vm_8c.html#a9a53bcd3cafc4494b5cd13f19d753a23',1,'vm.c']]],
  ['kvmmap_11',['kvmmap',['../d5/d64/defs_8h.html#a2ad79c7b5ecb41955635f0eafa67f641',1,'kvmmap(pagetable_t, uint64, uint64, uint64, int):&#160;vm.c'],['../de/de9/vm_8c.html#a0f19b589ef525301937990d7890c7eee',1,'kvmmap(pagetable_t kpgtbl, uint64 va, uint64 pa, uint64 sz, int perm):&#160;vm.c']]]
];
