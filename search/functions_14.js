var searchData=
[
  ['validatetest_0',['validatetest',['../de/dc0/usertests_8c.html#a8e8c52da79edb18a03fda8b69d798666',1,'usertests.c']]],
  ['virtio_5fdisk_5finit_1',['virtio_disk_init',['../d5/d64/defs_8h.html#a65e38631d3742246de68fb0dbf820613',1,'defs.h']]],
  ['virtio_5fdisk_5fintr_2',['virtio_disk_intr',['../d5/d64/defs_8h.html#adf67671b301a425c74cdbedfeaa9692d',1,'virtio_disk_intr(void):&#160;virtio_disk.c'],['../d9/d3e/virtio__disk_8c.html#afd6fb0a016a11952cd23c24c0a5dd4b5',1,'virtio_disk_intr():&#160;virtio_disk.c']]],
  ['virtio_5fdisk_5frw_3',['virtio_disk_rw',['../d5/d64/defs_8h.html#aac03d5e3619e9766d03e8fab66f48316',1,'virtio_disk_rw(struct buf *, int):&#160;virtio_disk.c'],['../d9/d3e/virtio__disk_8c.html#a72981a39d45349f6aad51738edecae7a',1,'virtio_disk_rw(struct buf *b, int write):&#160;virtio_disk.c']]],
  ['vprintf_4',['vprintf',['../d7/dea/user_2printf_8c.html#aff1aedc7c31f47a7a599156355c28d7b',1,'printf.c']]]
];
